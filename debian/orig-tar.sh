#!/bin/sh
set -e

PACKAGE=$(dpkg-parsechangelog -S Source)
VERSION=$2
DIR=${PACKAGE}-${VERSION}
TAR=../${PACKAGE}_${VERSION}.orig.tar.xz

rm $3
svn export svn://svn.code.sf.net/p/kdgcommons/code/tags/rel_$(echo $VERSION | tr '.' '_') $DIR
XZ_OPT=--best tar -c -v -J -f $TAR $DIR
rm -Rf $DIR
